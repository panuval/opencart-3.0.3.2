WITH RECURSIVE cte AS
(
  SELECT a.category_id, CAST(name AS CHAR(200)) AS name,
         CAST(a.category_id AS CHAR(200)) AS path,
         0 as depth
  FROM oc_category a, oc_category_description b WHERE parent_id = 0 AND a.category_id = b.category_id
  UNION ALL
  SELECT c.category_id,
         CONCAT(REPEAT('    ', cte.depth+1), ocd.name), # indentation
         CONCAT(cte.path, ",", c.category_id),
         cte.depth+1	
  FROM oc_category c JOIN cte ON
  			cte.category_id=c.parent_id
  		 JOIN oc_category_description ocd  ON ocd.category_id = c.category_id
),
CT2(category_id,_count) AS 
(	
	SELECT category_id,COUNT(*) AS _count FROM oc_product_to_category GROUP BY category_id
)
SELECT cte.*,ct2._count
FROM cte LEFT JOIN ct2 ON cte.category_id = ct2.category_id ORDER BY path;