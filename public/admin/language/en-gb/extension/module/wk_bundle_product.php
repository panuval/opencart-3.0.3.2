<?php

// Heading
$_['heading_title']    	  			    = 'Webkul Bundle Product';

// Text
$_['text_extension']      	  		    = 'Extension';
$_['text_success']        				= 'Success You have modified module Bundle Product !';
$_['text_edit']   		  				= 'Bundle Product Settings';

// Entry
$_['entry_status']                      = 'Status';

$_['entry_tax']                         = 'Tax';
$_['entry_tax_info']                    = 'Bundle Product tatal to be shown with applied tax';

$_['entry_per_page']                    = 'Bundle Limit Per-Page';
$_['entry_per_page_info']               = 'Number of bundles that should be displayed on Bundle page';

$_['entry_per_module']                  = 'Bundle Limit on Module';
$_['entry_per_module_info']             = 'Number of Bundles that should be displayed on Module.';

$_['entry_image_size']                  = 'Picture Width and Height';
$_['entry_image_size_info']             = 'Picture size should be in pixels';
$_['entry_pixel']                       = 'Pixel';

$_['title']							    = 'Title';
$_['entry_title']                       = 'Title For Bundle Page';
$_['entry_title_info']                  = 'Title for bundle product will be displayed on the Bundle page.';

$_['description']                       = 'Description';
$_['entry_description']                 = 'Description For Bundle Page';
$_['entry_description_info']            = 'Description For Bundle will be displyed on the Bundle page.';

$_['entry_add_product']                 = 'Add Product';
$_['entry_add_product_info']            = 'For create a Bundle product, you have to add atleast two products.';

$_['entry_discount_type']               = 'Discount';
$_['entry_quantity_per_customer']       = 'Fixed Quantity Per-Customer';
$_['entry_width']                       = 'Width';
$_['entry_height']                      = 'Height';
$_['entry_quantity']                    = 'Quantity';

// Error
$_['error_warning'] 		 			= 'Warning You do not have permission to modify Bundle Product Module !';
$_['error_permission']       = 'Warning: You do not have permission to modify bundle product!';

?>
