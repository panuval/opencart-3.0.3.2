<?php
/**
 * @author     Sainent eBusiness
 * @license    http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @version    1.0
 * @Website    http://www.sainent.com/
 */
// Heading
$_['heading_title']      = 'PayUbiz & PayU Money'; 

// Text 
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified PayUbiz & PayU Money account details!';
$_['text_payu_sainent']  = '<a href="https://www.payu.in" target="_blank"><img src="view/image/payment/payu_sainent_logo.png" alt="PayUbiz & PayU Money By Sainent" title="PayU By Sainent" style="height:40px;"/></a>';
$_['text_live']          = 'Live';
$_['text_demo']          = 'Test';
$_['text_successful']    = 'Always Successful';
$_['text_fail']          = 'Always Fail';

// Entry

$_['entry_merchantid1']  = 'Merchant Key For INR';
$_['entry_salt1']        = 'Salt For INR';
$_['entry_currency1']    = 'Currency Code INR';

$_['entry_merchantid']   = 'Merchant Key';
$_['entry_salt']         = 'Salt';
$_['entry_currency']   	 = 'Currency Code';

$_['help_merchantid']    = 'Enter the MerchantID value provided by PayU For the Currency Which you have Mention in Curreny  Field value.';
$_['help_currency']   	 = 'Enter the Currency which is related to the Key and Salt';
$_['help_salt']		     = 'Enter the Salt value provided by PayU For the Currency Which you have Mention in Curreny  Field value .';

$_['entry_test']         = 'Mode';
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Order Status:';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_pg']         	 = 'Payment Gateway';
$_['entry_status']       = 'Status'; 
$_['entry_sort_order']   = 'Sort Order';
$_['help_salt1']		 = 'Enter the Salt value provided by PayU For the Currency Which you have Mention in Curreny 1 Field value .';
$_['help_currency1']     = 'Enter the Currency which is related to the Key 1 and Salt 1';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment PayU!';
$_['error_merchant']     = 'Merchant ID Required!';
$_['error_salt']         = 'Salt Required!';

// Tab
$_['tab_general']					 = 'General';
$_['tab_order_status_payubiz']         = 'Order Status';

// Payu Status

$_['entry_captured_order_status']         = 'Captured Status';
$_['entry_auth_order_status']                    = 'Auth Status';
$_['entry_bounced_order_status']                 = 'Bounced Status';
$_['entry_dropped_order_status']                 = 'Dropped Status';
$_['entry_failed_order_status']                  = 'Failed Status';
$_['entry_user_cancelled_order_status']           = 'User Cancelled Status';
$_['entry_cancelled_order_status']               = 'Cancelled Status';
$_['entry_inprogress_order_status']              = 'In progress Status';
$_['entry_initiated_order_status']               = 'Initiated Status';
$_['entry_auto_refund_order_status']             = 'Auto Refund Status';
$_['entry_pending_order_status']                 = 'Pending Status';

//payu payment gateways

$_['entry_pg']         = 'Payment Gateway';
$_['entry_bankcode']         = 'Bank Code';


?>