<?php
/* Opencart Module v3.0.2.0. for PayUmoney and Citrus Payment ICP - Copyrighted file - Please do not modify/refactor/disasseble/extract any or all part content  */
// Heading
$_['heading_title']      	= 'PayUbiz'; 
// Text 
$_['text_payment']       	= 'Extensions';

$_['text_pumcp']      		= '<a onclick="window.open(\'https://www.payubiz.in\');"><img src="view/image/payment/payu.png" alt="PayUbiz" title="PayUbiz" style="border: 1px solid #EEEEEE;" height="25" /></a>';

$_['text_edit'] 			= 'Edit Module Parameters';

$_['entry_module']   		= 'Gateway Mode:';  
$_['entry_module_id'] 		= 'Sandbox|Production';
$_['entry_geo_zone']     	= 'Geo Zone:';
$_['entry_currency']		= 'Currency';
$_['entry_total']        	= 'Total';
$_['entry_order_status'] 	= 'Success Order Status:';
$_['entry_order_fail_status'] = 'Cancel/Fail Order Status:';
$_['text_disabled']  		= 'Disabled';
$_['text_enabled']  		= 'Enabled';
$_['entry_status']       	= 'Status:';
$_['entry_sort_order']   	= 'Sort Order:';
$_['text_success']       	= 'Success: You have successfully modified Payment settings';
$_['help_total']       		= 'Order total on which this payment option to be available for checkout.';
$_['help_currency']			= 'Three-letter ISO 4217 currency code required. e.g. INR,USG,GBP etc.';
// Entry PayUM
$_['entry_merchant']     	= 'Merchant Key:';
$_['entry_salt']         	= 'Salt:';
$_['help_salt']		     	= 'Enter the Salt value provided by PayUbiz.';
$_['help_merchant']		    = 'Enter the Merchant Key provided by PayUbiz.';
$_['error_merchant']     	= 'Merchant Key Required!';
$_['error_salt']         	= 'Salt Required!';

// Error
$_['error_permission']   	= 'Warning: You do not have permission to modify payment details!';

$_['error_currency']		= 'Currency Code Required.';

?>