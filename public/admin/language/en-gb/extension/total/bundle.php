<?php
// Heading
$_['heading_title']    = 'Bundle Discount';

// Text
$_['text_extension']   = 'Extension';
$_['text_success']     = 'Success: You have modified Bundle Discount!';
$_['text_edit']        = 'Edit Bundle Discount';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Bundle Discount!';