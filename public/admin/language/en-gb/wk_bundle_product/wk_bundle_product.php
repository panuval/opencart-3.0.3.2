<?php
// Heading
$_['heading_title']          = 'Manage Bundle Product';

// Text
$_['text_success']           = 'Success: You have modified Bundle Products !';
$_['text_list']              = 'Bundle Product List';
$_['text_add']               = 'Add Bundle Product';
$_['text_edit']              = 'Edit Bundle Product';
$_['text_default']           = 'Default';
$_['text_from']              = 'Valid From';
$_['text_to']                = 'Valid To';

// Column
$_['column_bundle_id']       = 'Bundle ID';
$_['column_name']            = 'Bundle Name';
$_['column_valid_from']      = 'Valid From';
$_['column_valid_to']        = 'Valid To';
$_['column_quantity']        = 'Quantity';
$_['column_sort_order']      = 'Sort Order';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_add_product']      = 'Add Products';
$_['entry_add_product_info'] = 'For bundle product, you have to add atleast two products.';

$_['entry_date']             = 'Date';
$_['entry_date_info']        = 'Select Start date and End date.';

$_['entry_title']            = 'Bundle Title';
$_['entry_title_info']       = 'Bundle title is the title for Bundle.';

$_['entry_unit_status']      = 'Units Status';
$_['entry_unit_status_info'] = 'Specify whether to show the units of bundle products or not.';

$_['entry_units_avail']      = 'Units Available';
$_['entry_units_avail_info'] = 'Specify the number of units available which is less than or equal to lowest units of selected products';

$_['entry_discount']         = 'Add Discount';
$_['entry_discount_info']    = 'Specify the discount on total order.';

$_['entry_current_status']   = 'Status';

$_['entry_sort_order']       = 'Sort Order';

$_['entry_discount_option']	 = 'Discount Option';
$_['text_percentage']		 = 'Percentage';
$_['text_fixed']			 = 'Fixed';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify bundle product!';
$_['error_units_ltz']        = 'Warning: Units available can not be less than or equal to zero.';
$_['error_units']            = 'Warning: Units available must be less than or equal to lowest units of selected products.';
$_['error_list']             = 'Warning: For a bundle product, you have to add atleast two products.';
$_['error_to']               = 'Warning: Please check the form carefully for errors!';
$_['error_form']             = 'Warning: Please check the form carefully for errors!';
$_['error_name']             = 'Please enter a valid name for bundle product!';
$_['error_name_exist']       = 'Bundle Title is already exist!';
$_['error_valid_from']       = 'Please enter a valid date!';
$_['error_valid_to']         = 'Please enter a valid date!';
$_['error_date']			 = 'End date should be greater than start date!';