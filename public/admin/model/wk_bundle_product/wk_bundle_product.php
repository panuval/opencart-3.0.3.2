<?php
class ModelWkBundleProductWkBundleProduct extends Model {


	/**
	 * [createTable method is used for creating databaase table for bundle products.]
	 * @return [type] [description]
	 */
	public function createTable() {
		$this->db->query("CREATE TABLE IF NOT EXISTS ".DB_PREFIX."bundle (
						bundle_id INT(11) NOT NULL AUTO_INCREMENT,
						name VARCHAR(1000),
						valid_from DATE,
						valid_to DATE,
						units_status BOOLEAN,
						units INT(11),
						discount_option BOOLEAN,
						discount DECIMAL(10,4),
						date_added DATETIME,
						date_modified DATETIME,
						sort_order INT(11),
						status BOOLEAN,
						PRIMARY KEY (bundle_id))
						");

		$this->db->query("CREATE TABLE IF NOT EXISTS ".DB_PREFIX."bundle_products (
						id INT(11) NOT NULL AUTO_INCREMENT,
						bundle_id INT(11),
						product_id INT(11),
						PRIMARY KEY (id))
						");

		$this->db->query("CREATE TABLE IF NOT EXISTS ".DB_PREFIX."bundle_cart (
						id INT(11) NOT NULL AUTO_INCREMENT,
						bundle_id INT(11),
						product_id INT(11),
						customer_id INT(11),
						session_id VARCHAR(1000),
						PRIMARY KEY (id))
						");
	}

	public function removeTable() {
		$this->db->query("DROP TABLE IF EXISTS `".DB_PREFIX ."bundle`");
		$this->db->query("DROP TABLE IF EXISTS `".DB_PREFIX ."bundle_products`");
		$this->db->query("DROP TABLE IF EXISTS `".DB_PREFIX ."bundle_cart`");
	}

	/**
	 * [addBundleProduct method is used for adding a new bundle product.]
	 * @param [array] $data [it contains all required values for a bundle product.]
	 */
	public function addBundleProduct($data) {

		$sql = "INSERT INTO ".DB_PREFIX."bundle SET name ='".$data['name']."' , valid_from ='".$this->db->escape($data['valid_from'])."' , valid_to ='".$this->db->escape($data['valid_to'])."' , units_status ='".$data['units_status']."' , units ='".($data['units']? $data['units'] : 0)."' ,discount ='".($data['discount']? $data['discount'] : 0)."',discount_option ='".$data['discount_option']."' , date_added =NOW() , date_modified=NOW(), sort_order='".($data['sort_order']? $data['sort_order'] : 0)."' , status='".$data['status']."'";

		$this->db->query($sql);

		$bundle_id = $this->db->getLastId();
		if(isset($data['list']['product_ids']) && $data['list']['product_ids']) {

			foreach ($data['list']['product_ids'] as $key => $value) {
				$sqlquery = "INSERT INTO ".DB_PREFIX."bundle_products SET bundle_id='".$bundle_id."' , product_id='".$value."'";
				$this->db->query($sqlquery);
			}
		}
	}

	/**
	 * [getBundleProducts method is used for getting all bundle products from database.]
	 * @return [array] [it returns records of bundle products.]
	 */
	public function getBundleProducts($data) {

		$sql = "SELECT * FROM ".DB_PREFIX."bundle WHERE 1 ";

		$sort_data = array(
			'bundle_id',
			'name',
			'valid_from',
			'valid_to',
			'units',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY bundle_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$result = $this->db->query($sql);

		return $result->rows;
	}

	/**
	 * [getBundleProductTotal method is used for getting total number of bundle products available in database.]
	 * @return [int] [it returns total number of available bundle products.]
	 */
	public function getBundleProductTotal() {

		$sql = "SELECT * FROM ".DB_PREFIX."bundle WHERE 1 ";

		$result = $this->db->query($sql);	

		return count($result->rows);
	}

	/**
	 * [deleteBundleProduct method is used for delete bundle product from bundle list.]
	 * @param  [int] $id [it contains bundle_id.]
	 * @return [type]     [description]
	 */
	public function deleteBundleProduct($id) {

		$this->db->query("DELETE FROM ".DB_PREFIX."bundle WHERE bundle_id='".$id."'");

		$this->db->query("DELETE FROM ".DB_PREFIX."bundle_products WHERE bundle_id='".$id."'");

	}

	/**
	 * [getBundleProductById description]
	 * @param  [type] $bundle_id [description]
	 * @return [type]            [description]
	 */
	public function getBundleProductById($bundle_id) {

		$result = $this->db->query("SELECT * FROM ".DB_PREFIX."bundle WHERE bundle_id='".$bundle_id."'")->row;

		$list = $this->db->query("SELECT bp.product_id,p.price,p.quantity,pd.name FROM ".DB_PREFIX."bundle bun LEFT JOIN ".DB_PREFIX."bundle_products bp ON (bun.bundle_id = bp.bundle_id) LEFT JOIN ".DB_PREFIX."product p ON (bp.product_id = p.product_id) LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id = pd.product_id)  WHERE bun.bundle_id = '".$bundle_id."'")->rows;

		foreach ($list as $key => $value) {
			$product_ids[$value['product_id']] = $value['product_id']; 
			$quantaties[$value['product_id']]  = $value['quantity'];
			$prices[$value['product_id']] = $value['price'];
			$product_name[$value['product_id']] = $value['name'];
		}

		$result['list']['product_ids'] = $product_ids;
		$result['list']['quantaties']  = $quantaties;
		$result['list']['prices']      = $prices;
		$result['list']['product_name']= $product_name;

	return $result;
	}

	/**
	 * [editBundleProduct method is used for edit the selected bundle product.]
	 * @param  [int] $bundle_id [this is bundle_id.]
	 * @param  [array] $data    [this is array.]
	 * @return [type]           [description]
	 */
	public function editBundleProduct($bundle_id, $data) {

		$sql = "UPDATE ".DB_PREFIX."bundle SET name ='".$data['name']."' , valid_from ='".$this->db->escape($data['valid_from'])."' , valid_to ='".$this->db->escape($data['valid_to'])."' , units_status ='".$data['units_status']."' , units ='".$data['units']."' ,discount_option ='".$data['discount_option']."' ,discount ='".$data['discount']."' , date_modified=NOW(), sort_order='".$data['sort_order']."' , status='".$data['status']."' WHERE bundle_id= '".$bundle_id."'";

		$this->db->query($sql);

		if(isset($data['list']['product_ids']) && $data['list']['product_ids']) {

			$this->db->query("DELETE FROM ".DB_PREFIX."bundle_products WHERE bundle_id='".$bundle_id."'");

			foreach ($data['list']['product_ids'] as $key => $value) {
				$sqlquery = "INSERT INTO ".DB_PREFIX."bundle_products SET bundle_id='".$bundle_id."' , product_id='".$value."'";
				$this->db->query($sqlquery);
			}
		}
	}
}
?>