<?php
class ControllerExtensionModuleWkBundleProduct extends Controller {

	private $error = array();


	/**
	 * [install function is used for creating database table.]
	 * @return [type] [description]
	 */
	public function install() {
		$this->load->model('wk_bundle_product/wk_bundle_product');
		$this->model_wk_bundle_product_wk_bundle_product->createTable();
	}

	public function uninstall() {
		$this->load->model('wk_bundle_product/wk_bundle_product');
		$this->model_wk_bundle_product_wk_bundle_product->removeTable();
	}


	public function index() {

		$data = array();
		$this->load->model('setting/setting');
		$this->load->model('wk_bundle_product/wk_bundle_product');
		$data = array_merge($data,$this->load->language('extension/module/wk_bundle_product'));
		$this->document->setTitle($data['heading_title']);

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_wk_bundle_product', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$config_data = array
				(
					'module_wk_bundle_product_status',
					'module_wk_bundle_product_per_module',
					'module_wk_bundle_product_per_page',
					'module_wk_bundle_product_image_width',
					'module_wk_bundle_product_image_height',
					'module_wk_bundle_product_title',
					'module_wk_bundle_product_description'

				);

		foreach ($config_data as $conf) {
			if (isset($this->request->post[$conf])) {
				$data[$conf] = $this->request->post[$conf];
			} else {
				$data[$conf] = $this->config->get($conf);
			}
		}


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if(isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_extension'),
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/wk_bundle_product', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$data['action'] = $this->url->link('extension/module/wk_bundle_product', 'user_token=' . $this->session->data['user_token'], true);
   		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

   		$data['user_token']  = $this->session->data['user_token'];

   		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		$data['column_left'] = $this->load->controller('common/column_left');

		$this->response->setOutput($this->load->view('extension/module/wk_bundle_product',$data));

	}

	/**
	 * [validate method is used for check current file has permission for modification or not.]
	 * @return [boolean] [it returns true if file has permission otherwise it reurns false.]
	 */
	protected function validate() {

		if (!$this->user->hasPermission('modify', 'extension/module/wk_bundle_product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
?>
