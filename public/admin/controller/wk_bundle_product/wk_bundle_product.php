<?php
class ControllerWkBundleProductWkBundleProduct extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('wk_bundle_product/wk_bundle_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('wk_bundle_product/wk_bundle_product');

		$this->getList();
	}

	public function add() {
		$this->language->load('wk_bundle_product/wk_bundle_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('wk_bundle_product/wk_bundle_product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_wk_bundle_product_wk_bundle_product->addBundleProduct($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'], true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->language->load('wk_bundle_product/wk_bundle_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('wk_bundle_product/wk_bundle_product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_wk_bundle_product_wk_bundle_product->editBundleProduct($this->request->get['bundle_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('wk_bundle_product/wk_bundle_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('wk_bundle_product/wk_bundle_product');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $wk_bundle_product_id) {
				$this->model_wk_bundle_product_wk_bundle_product->deleteBundleProduct($wk_bundle_product_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'bundle_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('wk_bundle_product/wk_bundle_product/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('wk_bundle_product/wk_bundle_product/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$wk_bundle_product_total = $this->model_wk_bundle_product_wk_bundle_product->getBundleProductTotal();

		$results = $this->model_wk_bundle_product_wk_bundle_product->getBundleProducts($filter_data);

		foreach ($results as $result) {

			$data['bundles'][] = array(
				'bundle_id'   => $result['bundle_id'],
				'name'        => $result['name'],
				'valid_from'  => $result['valid_from'],
				'valid_to'    => $result['valid_to'],
				'discount'    => $result['discount'],
				'quantity'    => $result['units'],
				'sort_order'  => $result['sort_order'],
				'status'      => ($result['status']? 'Enabled' : 'Disabled'),
				'edit'        => $this->url->link('wk_bundle_product/wk_bundle_product/edit', 'user_token=' . $this->session->data['user_token'] . '&bundle_id=' . $result['bundle_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$lang = $this->language->load('wk_bundle_product/wk_bundle_product');
		if(isset($lang['backup'])){
			unset($lang['backup']);
		}
		foreach ($lang as $key => $value) {
			$data[$key] = $this->language->get($value);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
		$data['sort_valid_from'] = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . '&sort=valid_from' . $url, true);
		$data['sort_valid_to'] = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . '&sort=valid_to' . $url, true);
		$data['sort_quantity'] = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . '&sort=units' . $url, true);
		$data['sort_status'] = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);
		$data['sort_bundle_id'] = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . '&sort=bundle_id' . $url, true);


		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $wk_bundle_product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);


		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($wk_bundle_product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($wk_bundle_product_total - $this->config->get('config_limit_admin'))) ? $wk_bundle_product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $wk_bundle_product_total, ceil($wk_bundle_product_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('wk_bundle_product/wk_bundle_product_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['bundle_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');


		$lang = $this->load->language('wk_bundle_product/wk_bundle_product');
		if(isset($lang['backup'])){
			unset($lang['backup']);
		}
		foreach ($lang as $key => $value) {
			$data[$key]  = $this->language->get($value);
		}


		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['valid_from'])) {
			$data['error_valid_from'] = $this->error['valid_from'];
		} else {
			$data['error_valid_from'] = array();
		}

		if (isset($this->error['valid_to'])) {
			$data['error_valid_to'] = $this->error['valid_to'];
		} else {
			$data['error_valid_to'] = '';
		}

		if (isset($this->error['list'])) {
			$data['error_list'] = $this->error['list'];
		} else {
			$data['error_list'] = '';
		}

		$post_array = array('name',
							'valid_from',
							'valid_to',
							'units_status',
							'units',
							'discount_option',
							'discount',
							'list',
							'sort_order',
							'status'
							);

		if(isset($this->request->get['bundle_id'])) {
			$result = $this->model_wk_bundle_product_wk_bundle_product->getBundleProductById($this->request->get['bundle_id']);
		}

		foreach ($post_array as $key => $value) {

			if(isset($this->request->post[$value]) && !isset($this->request->get['bundle_id'])) {
				$data[$value] = $this->request->post[$value];
			} else if(isset($this->request->get['bundle_id'])) {
				$data[$value] = $result[$value];
 			} else {
					$data[$value] = '';
			}

		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (!isset($this->request->get['bundle_id'])) {
			$data['action'] = $this->url->link('wk_bundle_product/wk_bundle_product/add', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('wk_bundle_product/wk_bundle_product/edit', 'user_token=' . $this->session->data['user_token'] . '&bundle_id=' . $this->request->get['bundle_id'], true);
		}

		$data['cancel'] = $this->url->link('wk_bundle_product/wk_bundle_product', 'user_token=' . $this->session->data['user_token'], true);



		$data['user_token'] = $this->session->data['user_token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('wk_bundle_product/wk_bundle_product_form', $data));
	}

	protected function validateForm() {

		$date_from = date_create($this->request->post['valid_from']);
		$date_to = date_create($this->request->post['valid_to']);

		$diff1 = date_diff($date_from,$date_to);
		$temp1 = $diff1->format("%R%a");

		if (!$this->user->hasPermission('modify', 'wk_bundle_product/wk_bundle_product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(!isset($this->request->post['name']) || empty($this->request->post['name']) ) {
			$this->error['warning'] = $this->language->get('error_form');
			$this->error['name'] = $this->language->get('error_name');
		}

		if(!isset($this->request->post['valid_from']) || empty($this->request->post['valid_from'])) {
			$this->error['warning'] = $this->language->get('error_form');
			$this->error['valid_from'] = $this->language->get('error_valid_from');
		}

		if(!isset($this->request->post['valid_to']) || empty($this->request->post['valid_to'])) {
			$this->error['warning'] = $this->language->get('error_to');
			$this->error['valid_to'] = $this->language->get('error_valid_to');
		}

		if (isset($this->request->post['valid_from']) && isset($this->request->post['valid_to']) && $temp1 < 0) {
			$this->error['warning'] = $this->language->get('error_to');
			$this->error['valid_to'] = $this->language->get('error_date');
		}

		if (isset($this->request->post['list']['quantaties']) && $this->request->post['list']['quantaties']) {
			if(count($this->request->post['list']['quantaties']) <= 1) {
				$this->error['warning'] = $this->language->get('error_form');
				$this->error['list'] = $this->language->get('error_list');
			}
		} else {
			$this->error['warning'] = $this->language->get('error_form');
			$this->error['list'] = $this->language->get('error_list');
		}

		if(isset($this->request->post['list']['quantaties']) && isset($this->request->post['units']) && $this->request->post['list']['quantaties'] && $this->request->post['units']) {
			if(min($this->request->post['list']['quantaties']) < $this->request->post['units']) {
				$this->error['warning'] = $this->language->get('error_units');
			}
		}

		if (isset($this->request->post['units']) && $this->request->post['units'] < 1) {
			$this->error['warning'] = $this->language->get('error_units_ltz');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'wk_bundle_product/wk_bundle_product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	/**
	 * [autocomplete method is used for display listing of all the products for create a bundle.]
	 * @return [array] [it returns all the products on the basis of search with product_id,product name.]
	 */
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/product');
			$this->load->model('catalog/option');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				$option_data = array();

				$product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

				foreach ($product_options as $product_option) {
					$option_info = $this->model_catalog_option->getOption($product_option['option_id']);

					if ($option_info) {
						$product_option_value_data = array();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

							if ($option_value_info) {
								$product_option_value_data[] = array(
									'product_option_value_id' => $product_option_value['product_option_value_id'],
									'option_value_id'         => $product_option_value['option_value_id'],
									'name'                    => $option_value_info['name'],
									'price'                   => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
									'price_prefix'            => $product_option_value['price_prefix']
								);
							}
						}

						$option_data[] = array(
							'product_option_id'    => $product_option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $product_option['option_id'],
							'name'                 => $option_info['name'],
							'type'                 => $option_info['type'],
							'value'                => $product_option['value'],
							'required'             => $product_option['required']
						);
					}
				}

				$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'model'      => $result['model'],
					'option'     => $option_data,
					'price'      => $result['price'],
					'quantity'   => $result['quantity']
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}
