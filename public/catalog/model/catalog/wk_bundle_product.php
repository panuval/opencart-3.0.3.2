<?php
class ModelCatalogWkBundleProduct extends Model
{

	/**
	 * [getBundleProducts method is used for getting all the enabled bundles and this bundles will
	 * be showing on module or product page.]
	 * @param  [array] $data [it contains filter values by which it will get the bundles.]
	 * @return [array]       [it returns array of bundle records.]
	 */
	public function getBundleProducts($data) 
	{
		$now = date('Y-m-d');
		$sql = "SELECT * FROM ".DB_PREFIX."bundle WHERE status='1' AND (valid_from <= '".$now."' AND valid_to >= '".$now."') and (units > 0)";

		$sort_data = array(
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 10;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$result = $this->db->query($sql)->rows;

		if(isset($result)) {
			foreach ($result as $key => $values) {

				$bundles = $this->db->query("SELECT p.product_id,pd.description,p.image,p.price,p.tax_class_id,pd.name,p.minimum,(SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "bundle_products bp LEFT JOIN ".DB_PREFIX."product p ON (bp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND bundle_id='".$values['bundle_id']."'");
				$result[$key]['bundles'] = $bundles->rows;
			}
		}

		return $result;
	}

	/**
	 * [getBundleProductsTotal method is used for getting total number of bundles available in database.]
	 * @return [int] [it returns total number of records of bundle.]
	 */
	public function getBundleProductsTotal() {
		$now = date('Y-m-d');

		$sql = "SELECT * FROM ".DB_PREFIX."bundle WHERE status='1' AND (valid_from <= '".$now."' AND valid_to >= '".$now."') AND (units > 0) ORDER BY sort_order ASC";
		$result = $this->db->query($sql);
		return count($result->rows);

	}

	/**
	 * [getBundleProductsByID method is used for getting all the bundle products related to a particular product.]
	 * @param  [int] $product_id [this is product_id of a particular product.]
	 * @return [array]           [it returns array.]
	 */
	public function getBundleProductsByID($product_id, $limit) {

		if($limit < 0) {
			$limit = 0;
		}
		
		$now = date('Y-m-d');

		$result = $this->db->query("SELECT * FROM ".DB_PREFIX."bundle bu LEFT JOIN ".DB_PREFIX."bundle_products bp ON (bu.bundle_id = bp.bundle_id) WHERE status='1' AND (bu.valid_from <= '".$now."' AND bu.valid_to >= '".$now."') AND (bu.units > 0) AND bp.product_id='".$product_id."' ORDER BY bu.sort_order ASC LIMIT ". (int)$limit)->rows;

		if(isset($result)) {
			foreach ($result as $key => $values) {

				$bundles = $this->db->query("SELECT p.product_id,pd.description,p.image,p.price,p.tax_class_id,pd.name,p.minimum,(SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "bundle_products bp LEFT JOIN ".DB_PREFIX."product p ON (bp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND bundle_id='".$values['bundle_id']."'");
				$result[$key]['bundles'] = $bundles->rows;
			}
		}
		return $result;
	}

	/**
	 * [getBundleProductById method is used for getting all the products of the bundle with available quantity.]
	 * @param  [int] $bundle_id [this is bundle_id.]
	 * @return [returns]        [it returns complete detials of bundles with product quantity.]
	 */
	public function getMinQuantityBundle($bundle_id) {

		$result = $this->db->query("SELECT * FROM ".DB_PREFIX."bundle WHERE bundle_id='".$bundle_id."'")->row;

		$list = $this->db->query("SELECT bp.product_id,p.price,p.quantity,pd.name FROM ".DB_PREFIX."bundle bun LEFT JOIN ".DB_PREFIX."bundle_products bp ON (bun.bundle_id = bp.bundle_id) LEFT JOIN ".DB_PREFIX."product p ON (bp.product_id = p.product_id) LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id = pd.product_id)  WHERE bun.bundle_id = '".$bundle_id."'")->rows;

		foreach ($list as $key => $value) {
			$product_ids[$value['product_id']] = $value['product_id']; 
			$quantaties[$value['product_id']]  = $value['quantity'];
			$prices[$value['product_id']] = $value['price'];
			$product_name[$value['product_id']] = $value['name'];
		}

		$result['list']['product_ids'] = $product_ids;
		$result['list']['quantaties']  = $quantaties;
		$result['list']['prices']      = $prices;
		$result['list']['product_name']= $product_name;

	return $result;
	}

	/**
	 * [updateQuantityofBundle method is used for update the quanity of bundle after successfully  create order.]
	 * @param  [int] $bundle_id          [this is bundle_id.]
	 * @param  [int] $minusquantity      [this is quantity of a product which buy by a customer.]
	 * @param  [int] $available_quantity [this is availble quantity of a bundle.]
	 * @return [type]                     [description]
	 */
	public function updateQuantityofBundle($bundle_id) {
		$this->db->query("UPDATE ".DB_PREFIX."bundle SET units=units-1 WHERE bundle_id='".(int)$bundle_id."' AND units>0");
	}


	/**
	 * [deleteBundleCart method is used for delete bundle_id from database.]
	 * @return [type] [descri]
	 */
	public function deleteBundleCart() {
		if($this->customer->getId()) {
			$customer_id = $this->customer->getId();
		} else {
			$customer_id = 0;
		}

		$this->db->query("DELETE FROM ".DB_PREFIX."bundle_cart WHERE customer_id=".$customer_id);
	}

	/**
	 * [deleteBundleCartByBundleId method is used for delete bundle cart by bundle_id, this method is used when cart product
	 * has been deleted from cart.]
	 * @param  [int] $bundle_id [this is bundle_id whose product has been deleted from the cart.]
	 * @return [type]            [description]
	 */
	public function deleteBundleCartByBundleId($bundle_id) {
		$this->db->query("DELETE FROM ".DB_PREFIX."bundle_cart WHERE bundle_id='".$bundle_id."' AND customer_id='".$this->customer->getId()."'");
	}

	/**
	 * [getBundle method is used for getting bundle using bundle_id.]
	 * @param  [int] $bundle_id [it is bundle_id for a bundle.]
	 * @return [array]          [it returns record of a bundle.]
	 */
	public function getBundle($bundle_id) {
		$result = $this->db->query("SELECT product_id FROM ".DB_PREFIX."bundle_products WHERE bundle_id='".$bundle_id."'")->rows;
		return $result;
	}

	/**
	 * [getBundleCart method is used for getting all the bundlesId which available in database.]
	 * @return [array] [it returns array of all bundle_id.]
	 */
	public function getBundleCart() {
		$result = $this->db->query("SELECT DISTINCT(bundle_id) FROM ".DB_PREFIX."bundle_cart")->rows;
		return $result;
	}

	/**
	 * [getCartProductByCartId method is used for getting a product available in cart by cart_id.]
	 * @param  [int] $cart_id [this is cart_id.]
	 * @return [array]        [it returns array of product which available in cart.]
	 */
	public function getCartProductByCartId($cart_id) {
		$sql = "SELECT * FROM ".DB_PREFIX."cart WHERE cart_id='".(int)$cart_id."'";
		$result = $this->db->query($sql)->row;
		return $result;
	}

	/**
	 * [getCartProductBuyQuntity method is used for getting quantity of a product which is available in cart for purchasing.]
	 * @param  [int] $product_id [product_id.]
	 * @return [array]           [it returns quantity of a product.]
	 */
	public function getCartProductBuyQuntity($product_id){
		$result = $this->db->query("SELECT quantity FROM ".DB_PREFIX."cart WHERE product_id='".(int)$product_id."'")->row;
		return $result;
	}

	/**
	 * [getBundlesByProductId method is used for get all the bundle in which a specific product is available.]
	 * @param  [int] $product_id [product_id]
	 * @return [array]           [it returns all the bundle_id which contains a particular product.]
	 */
	public function getBundlesByProductId($product_id) {
		$sql = "SELECT bundle_id FROM ".DB_PREFIX."bundle_products WHERE product_id='".(int)$product_id."'";
		$result = $this->db->query($sql)->rows;	
		return $result;
	}

	public function getBundleByProductId($product_id) {
		
		if($this->customer->getId()) {
			$customer_id = $this->customer->getId();
		} else {
			$customer_id = 0;
		}

		$query = $this->db->query("SELECT bundle_id FROM ".DB_PREFIX."bundle_cart WHERE product_id = ".$product_id." AND customer_id = ".$customer_id)->rows;

		return $query;
	}

	public function getCartIdsByBundleId($bundle_id) {
		$query = $this->db->query("SELECT c.cart_id FROM ".DB_PREFIX."bundle_products bp LEFT JOIN ".DB_PREFIX."cart c ON (c.product_id = bp.product_id) WHERE c.customer_id = '".$this->customer->getId()."' AND bp.bundle_id = '".$bundle_id."'")->rows;
		return $query;
	}

	public function getProductQuantity($cart_id) {
		$query = $this->db->query("SELECT quantity FROM ".DB_PREFIX."cart WHERE cart_id = ".$cart_id)->row;

		return $query;
	}

	public function updateProductQuantity($cart_id) {
		$this->db->query("UPDATE ".DB_PREFIX."cart SET quantity = quantity-1 WHERE cart_id='".(int)$cart_id."'");
	}
}
?>