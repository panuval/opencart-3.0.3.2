<?php
class ModelCatalogBundle extends Model
{
	/**
	 * [getBundle method is used for getting bundle using bundle_id.]
	 * @param  [int] $bundle_id [it is bundle_id for a bundle.]
	 * @return [array]          [it returns record of a bundle.]
	 */
	public function getProductsInBundle($bundle_id) 
	{
		$result = $this->db->query("SELECT product_id FROM ".DB_PREFIX."bundle_products WHERE bundle_id='".$bundle_id."'")->rows;
		return $result;
	}

	/**
	 * [getBundleDetails method provides all the details related to a bundle such as bundle-title,bundle-discount, etc. ]
	 * @param  [int] $bundle_id [this is bundle_id of a bundle.]
	 * @return [array]          [it returns all the details related to a bundle.]
	 */
	public function getBundleDetails($bundle_id) 
	{
		$result = $this->db->query("SELECT * FROM ".DB_PREFIX."bundle WHERE bundle_id='".$bundle_id."'")->row;
		return $result;
	}

	/**
	 * [addBundleToCart method is used for storing bundle_id into the database for later use in calculation of discount.]
	 * @param [int] $bundle_id  [it contains bundle_id of a bundle.]
	 * @param [int] $product_id [it contains product_id of a bundle.]
	 */
	public function addBundleToCart($bundle_id,$product_id) {

		$this->db->query("INSERT INTO ".DB_PREFIX."bundle_cart SET bundle_id='".$bundle_id."', product_id='".$product_id."', customer_id='".$this->customer->getId()."', session_id='".$this->session->getId()."'");
	}

	/**
	 * [deleteBundleCart method is used for delete bundle_id from database.]
	 * @return [type] [descri]
	 */
	public function deleteBundleCart() {
		if($this->customer->getId()) {
			$customer_id = $this->customer->getId();
		} else {
			$customer_id = 0;
		}
		$this->db->query("DELETE FROM ".DB_PREFIX."bundle_cart WHERE customer_id = ". $customer_id);
	}
}
?>