<?php
class ModelExtensionTotalBundle extends Model {
	/**
	 * [getTotal method is used for getting discount on the basis of bundle_id store in the database.]
	 * @param  [type] &$total_data [description]
	 * @param  [type] &$total      [description]
	 * @param  [type] &$taxes      [description]
	 * @return [type]              [description]
	 */
	public function getTotal($total) {

		$this->load->language('extension/total/bundle');

		if($this->cart->hasProducts()) {
			$cart = array();
			$cart_products = $this->cart->getProducts();
			foreach ($cart_products as $product) {
				$cart[$product['product_id']] = $product;
			}
		}

		if ($this->customer->getId()) {
			 $customer_id = $this->customer->getId();
		} else {
			$customer_id = 0;
		}

		$bundle_ids = $this->db->query("SELECT bundle_id,product_id FROM ".DB_PREFIX."bundle_cart WHERE customer_id=".$customer_id." AND session_id='".$this->session->getId()."'")->rows;

		if($bundle_ids) {
			$bundles = array();
			$total_discount = 0;
			if(!empty($bundle_ids)) {
				foreach ($bundle_ids as $key => $value) {
				    $bundles[$value['bundle_id']][] = $value['product_id'];
				}
			
				foreach ($bundles as $key => $value) {
						$discount = $this->db->query("SELECT discount, discount_option FROM ".DB_PREFIX."bundle WHERE bundle_id ='".$key."'")->row;
						if(isset($cart) && $cart) {

						if($discount['discount_option'] && $value) {
							$count = array_count_values($value);

							$count = $count[$value[0]];
							$total_discount = $total_discount + ($discount['discount'] > 0 ? $count*$discount['discount'] : 0);

						} elseif($value && isset($cart[$value[0]])) {
							foreach ($value as $key1 => $value1) {
							if(isset($cart[$value1])) {

								$total_discount = $total_discount + ($discount['discount'] > 0 ? ($cart[$value1]['price']*$discount['discount'] / 100) : 0);
							}
							}

						}
					}
				}
			}
		}


		if(isset($total_discount) && $total_discount > 0) {

			$total['totals'][] = array(
			'code'       => 'bundle',
			'title'      => $this->language->get('text_bundle'),
			'value'      => -$total_discount,
			'sort_order' => $this->config->get('total_bundle_sort_order')
			);
			$total['total'] = $total['total'] - $total_discount;

		}
	}
}
