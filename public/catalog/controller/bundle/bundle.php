<?php
class ControllerBundleBundle extends Controller {
	private $error = array();

	public function index() {
		$data = array();

		$this->load->model('catalog/bundle');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$data = array_merge($data,$this->load->language('product/product'));
		$data = array_merge($data,$this->load->language('bundle/bundle'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		if (isset($this->request->get['bundle_id'])) {
			$bundle_id = (int)$this->request->get['bundle_id'];
		} else {
			$bundle_id = 0;
		}
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_bundle'),
			'href' => $this->url->link('bundle/bundle','&bundle_id=' . $bundle_id,true)
		);

		$products = $this->model_catalog_bundle->getProductsInBundle($bundle_id);

		if($products) {

			$bundle_details = $this->model_catalog_bundle->getBundleDetails($bundle_id);

			$this->document->setTitle($bundle_details['name']);
			$total = 0;
			$total_price = 0;
			foreach ($products as $key => $product) {

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);
				
				if($product_info) {
					$data['bundle_id'] = $bundle_id;
					$data['products'][$product['product_id']]['product_name'] = $product_info['name'];
					$data['products'][$product['product_id']]['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
					$data['products'][$product['product_id']]['product_id'] = (int)$product['product_id'];
					$data['products'][$product['product_id']]['manufacturer'] = $product_info['manufacturer'];
					$data['products'][$product['product_id']]['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
					$data['products'][$product['product_id']]['model'] = $product_info['model'];
					$data['products'][$product['product_id']]['reward'] = $product_info['reward'];
					$data['products'][$product['product_id']]['points'] = $product_info['points'];
					$data['products'][$product['product_id']]['href'] = $this->url->link('product/product', 'product_id=' . $product['product_id']);

					$total_price += ($product_info['price'] * $product_info['minimum']);
									   
					if ($product_info['quantity'] <= 0) {
						$data['products'][$product['product_id']]['stock'] = $product_info['stock_status'];

					} elseif ($this->config->get('config_stock_display')) {
						$data['products'][$product['product_id']]['stock'] = $product_info['quantity'];

					} else {
						$data['products'][$product['product_id']]['stock'] = $this->language->get('text_instock');
					}

					if ($product_info['image']) {
						$data['products'][$product['product_id']]['thumb'] = $this->model_tool_image->resize($product_info['image'], 50, 50);
					} else {
						$data['products'][$product['product_id']]['thumb'] = $this->model_tool_image->resize('no_image.png', 50,50);;
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$data['products'][$product['product_id']]['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency']);
					} else {
						$data['products'][$product['product_id']]['price'] = false;
					}


					if ((float)$product_info['special']) {
						$data['products'][$product['product_id']]['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency']);
						$total += ($product_info['special'] * $product_info['minimum']);
					} else {
						$total += ($product_info['price'] * $product_info['minimum']);
						$data['products'][$product['product_id']]['special'] = false;
					}

					if ($this->config->get('config_tax')) {
						$data['products'][$product['product_id']]['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'],$this->session->data['currency']);
					} else {
						$data['products'][$product['product_id']]['tax'] = false;
					}

					$discounts = $this->model_catalog_product->getProductDiscounts($product['product_id']);

					$data['products'][$product['product_id']]['discounts'] = array();

					foreach ($discounts as $discount) {
						$data['products'][$product['product_id']]['discounts'][] = array(
							'quantity' => $discount['quantity'],
							'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency'])
						);
					}

					$data['products'][$product['product_id']]['options'] = array();

					foreach ($this->model_catalog_product->getProductOptions($product['product_id']) as $option) {
						$product_option_value_data = array();

						foreach ($option['product_option_value'] as $option_value) {
							if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
									$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false),$this->session->data['currency']);
								} else {
									$price = false;
								}

								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
									'price'                   => $price,
									'price_prefix'            => $option_value['price_prefix']
								);
							}
						}

						$data['products'][$product['product_id']]['options'][] = array(
							'product_option_id'    => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $option['option_id'],
							'name'                 => $option['name'],
							'type'                 => $option['type'],
							'value'                => $option['value'],
							'required'             => $option['required']
						);
					}

					if ($product_info['minimum']) {
						$data['products'][$product['product_id']]['minimum'] = $product_info['minimum'];
					} else {
						$data['products'][$product['product_id']]['minimum'] = 1;
					}
				}
			}
		} else {
			$this->document->setTitle($data['text_not_found']);
		}

		$data['discount'] = $this->currency->format(((float)$bundle_details['discount']),$this->session->data['currency']);
		$data['total_price']   = $this->currency->format((float)$total_price,$this->session->data['currency']);
		$data['discount_per'] = $this->currency->format(((float)$bundle_details['discount']*(float)$total/100),$this->session->data['currency']);
		$save_price = 0;
		if ($bundle_details['discount_option'] == 0) {
			$final_price = (float)$total-((float)$bundle_details['discount']*(float)$total/100);
			$data['price'] = $this->currency->format(($final_price),$this->session->data['currency']);
		} else {
			$final_price = (float)$total-(float)$bundle_details['discount'];
			$data['price'] = $this->currency->format(($final_price),$this->session->data['currency']);
		}
		$data['save_price'] = $this->currency->format(((float)$total_price-((float)$final_price)),$this->session->data['currency']);
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('bundle/bundle', $data));
	}

}
