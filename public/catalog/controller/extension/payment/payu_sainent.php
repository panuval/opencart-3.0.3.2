<?php
/**
 * @author     Sainent eBusiness
 * @license    http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @version    1.0
 * @Website    http://www.sainent.com/
 */
 
class ControllerExtensionPaymentPayuSainent extends Controller {

	public function index() {	
    	$this->load->model('checkout/order');
		$this->language->load('extension/payment/payu_sainent');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $currency_code = $order_info['currency_code'];

		$payu_merchantids = $this->config->get('payment_payu_sainent_merchantid');
		$payu_salts = $this->config->get('payment_payu_sainent_salt');
		$payu_currencies = $this->config->get('payment_payu_sainent_currency');	
		
		if($currency_code == $this->config->get('payment_payu_sainent_currency1')){
			$key = $this->config->get('payment_payu_sainent_merchantid1');        		
			$salt = $this->config->get('payment_payu_sainent_salt1');
		} else {
			foreach ($payu_currencies as $index => $value) {
				if($value == $currency_code){
					$key = $payu_merchantids[$index];        		
					$salt = $payu_salts[$index];
				}
			}
		}
		
        if(isset($key, $salt)){ 
			$data['merchant'] = $key;
	          
			$total = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
			if($order_info['currency_code'] != 'INR'){
				$calculatedAmount_INR = $this->currency->convert( $total, $order_info['currency_code'],'INR');
			}else{
				$calculatedAmount_INR = $total;
			}
			$calculatedAmount_INR = number_format((float)$calculatedAmount_INR, 2,'.','');	
			if($this->config->get('payment_payu_sainent_test')=='demo'){
				$data['action'] = 'https://test.payu.in/_payment.php';
				$txnid        = 	$this->session->data['order_id'];
			}else{
				$data['action'] = 'https://secure.payu.in/_payment.php';
				$txnid        = 	$this->session->data['order_id'];
			}        
			$data['key'] = $key;
			$data['salt'] = $salt;
			$data['txnid'] = $txnid;
			$data['amount'] = $calculatedAmount_INR;
			$data['productinfo'] = 'opencart products information';
			$data['firstname'] = $order_info['payment_firstname'];
			$data['lastname'] = $order_info['payment_lastname'];
			$data['zipcode'] = $order_info['payment_postcode'];
			$data['email'] = $order_info['email'];
			$data['phone'] = $order_info['telephone'];
			$data['address1'] = $order_info['payment_address_1'];
	        $data['address2'] = $order_info['payment_address_2'];
	        $data['state'] = $order_info['payment_zone'];
	        $data['city']=$order_info['payment_city'];
	        $data['country']=$order_info['payment_country'];
	        $data['pg']= '';
	        $data['bankcode']=$this->config->get('payment_payu_sainent_bankcode_val');
			$data['service_provider'] = $this->config->get('payment_payu_sainent_payment_gateway');
			
	        $data['surl'] = $this->url->link('extension/payment/payu_sainent/callback');
	        $data['furl'] = $this->url->link('extension/payment/payu_sainent/callback');
			$data['curl'] = $this->url->link('extension/payment/payu_sainent/callback'); 
			$key          =  $key;
			$amount       = (int)$order_info['total'];
			$productInfo  = $data['productinfo'];
		    $firstname    = $order_info['payment_firstname'];
			$email        = $order_info['email'];
			$salt         = $salt;
			
			$Hash=hash('sha512', $key.'|'.$txnid.'|'.$calculatedAmount_INR.'|'.$productInfo.'|'.$firstname.'|'.$email.'|||||||||||'.$salt); 
			
			$data['hash'] = $Hash;
	    } else {
			return '<h4 style="color:red">WARNING: Something Went wrong with configuration Please put Key and salt with refrence to Curreny: '.$currency_code.' !!</h4>';
	    }
		
		$data['button_confirm'] = $this->language->get('button_confirm');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/payment/payu_sainent')) {
			return $this->load->view($this->config->get('config_template') . '/template/extension/payment/payu_sainent', $data);
		} else {
			return $this->load->view('extension/payment/payu_sainent', $data);
		}
	}
	
	public function callback() {
		$key = $this->config->get('payment_payu_sainent_merchantid1');
		$salt = $this->config->get('payment_payu_sainent_salt1');
		$command = 'verify_payment';
		// $var1 = $this->session->data['order_id'];
		// $txnid = $this->session->data['order_id'];

		$var1 = $this->request->post['txnid'];
		$txnid = $this->session->data['order_id'];
		
		$hash = strtolower(hash('sha512', $key.'|'.$command.'|'.$var1.'|'.$salt));
		
		if($this->config->get('payment_payu_sainent_test')=='demo'){
			$url = "https://test.payu.in/merchant/postservice.php?form=2";
		}else{
			$url = "https://info.payu.in/merchant/postservice.php?form=2";
		}
		
		$ch = curl_init();
		
		$post = array(
			'key' => $key,
			'command' => $command,
			'var1' => $var1,
			'hash' => $hash,
		);
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);  //Post Fields
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);

		curl_close ($ch);
		
		$curloutput = json_decode($server_output,true);
		
		$status = '';
		$unmappedstatus = '';
		$mihpayid = '';
		
		if (isset($curloutput['transaction_details'][$txnid]['status'])) {
			$status = $curloutput['transaction_details'][$txnid]['status'];
			$unmappedstatus = $curloutput['transaction_details'][$txnid]['unmappedstatus'];
			$mihpayid = $curloutput['transaction_details'][$txnid]['mihpayid'];
		}
		
		$this->load->model('checkout/order');
		
		$this->language->load('extension/payment/payu_sainent');
		
		$data['title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));
		$data['charset'] = $this->language->get('charset');
		$data['language'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');
			
		$data['continue'] = $this->url->link('checkout/cart');
		$data['text_response'] = $this->language->get('text_pending');
		$data['text_wait'] = sprintf($this->language->get('text_wait'), $this->url->link('checkout/cart'));
		
		if (!isset($this->request->server['HTTPS']) || ($this->request->server['HTTPS'] != 'on')) {
			$data['base'] = HTTP_SERVER;
		} else {
			$data['base'] = HTTPS_SERVER;
		}
		
		$order_info = $this->model_checkout_order->getOrder($txnid);
        
		$order_id = (int)$order_info['order_id'];
		
		if ($order_id) {
			$message = '';
			$message .= 'OrderId: ' . $order_id . "\n";
			$message .= 'Transaction Id: ' . $mihpayid . "\n";
			$message .= 'Status: ' . $status . "\n";
			
			if ($status == 'success') {
				$data['continue'] = $this->url->link('checkout/success');
				$data['text_wait'] = sprintf($this->language->get('text_wait'), $this->url->link('checkout/success'));
				$data['text_response'] = $this->language->get('text_success');
				$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_captured_order_status_id'), $message, true);
			} else if ($status == 'pending') {
				$data['text_response'] = $this->language->get('text_pending');
				$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_inprogress_order_status_id'), $message, true);						
			} else if ($status == 'failure') {
				$data['text_response'] = $this->language->get('text_failure');
				$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_failed_order_status_id'), $message, true);
			} else {
				if($unmappedstatus != ''){
					$data['text_response'] = $this->language->get('text_unmappedstatus').$unmappedstatus;
					if($unmappedstatus == 'userCancelled'){
						$data['text_response'] = $this->language->get('text_cancelled');
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_user_cancelled_order_status_id'));
					} elseif ($unmappedstatus == 'auth'){
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_auth_order_status_id'));
					} elseif ($unmappedstatus == 'initiated'){	
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_initiated_order_status_id'));
					} elseif ($unmappedstatus == 'in progress'){
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_inprogress_order_status_id'));
					} elseif ($unmappedstatus == 'dropped'){
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_dropped_order_status_id'));
					} elseif ($unmappedstatus == 'bounced'){
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_bounced_order_status_id'));
					} elseif ($unmappedstatus == 'pending'){
						$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payu_sainent_pending_order_status_id'));
					}
				}
			}
		}
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/payment/payu_sainent_response')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/extension/payment/payu_sainent_response', $data));
		} else {
			$this->response->setOutput($this->load->view('extension/payment/payu_sainent_response', $data));
		}	 	
	}
}
?>