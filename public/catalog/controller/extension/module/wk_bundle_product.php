<?php
class ControllerExtensionModuleWkBundleProduct extends Controller {

	private $error = array();
	private $data = array();

	/**
	 * [index method is used for displaying all the bundle products.]
	 * @return [type] [description]
	 */
	public function index() {

		if (!$this->config->get('module_wk_bundle_product_status')) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->document->addStyle('catalog/view/theme/default/stylesheet/bundle/bundle.css');

		$this->language->load('extension/module/wk_bundle_product');
		$this->load->model('tool/image');
		$this->load->model('catalog/wk_bundle_product');

		$data['button_cart']=$this->language->get('button_cart');
		$data['button_clear_cart'] = $this->language->get('button_clear_cart');

		$data['text_price'] = $this->language->get('text_price');
		$data['text_save']  = $this->language->get('text_save');

		if ($this->config->get('total_bundle_status')) {
			$data['bundle_discount_status'] = true;
		} else {
			$data['bundle_discount_status'] = false;
		}

		if($this->config->get('module_wk_bundle_product_title')) {
				$data['bundle_page_title'] =html_entity_decode($this->config->get('module_wk_bundle_product_title'), ENT_QUOTES, 'UTF-8');
		} else {
			$data['bundle_page_title'] ='';
		}

		if($this->config->get('module_wk_bundle_product_description') && $this->config->get('module_wk_bundle_product_description')) {
			$data['bundle_page_description'] = html_entity_decode($this->config->get('module_wk_bundle_product_description'), ENT_QUOTES, 'UTF-8');
		} else {
			$data['bundle_page_description'] = false;
		}

		// this is used for set layout of bundle product page.
		if(isset($this->request->get['route']) AND (substr($this->request->get['route'],0,34)=='extension/module/wk_bundle_product')) {

			$this->document->setTitle($this->language->get('heading_title'));

	      	$data['breadcrumbs'] = array();

	      	$data['breadcrumbs'][] = array(
	        	'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home')
	      	);

	      	$data['breadcrumbs'][] = array(
	        	'text'      => $this->language->get('text_bundles'),
				'href'      => $this->url->link('extension/module/wk_bundle_product','' ,true)
	      	);

	      	if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$filter_data = array(
				'sort'  => 'sort_order',
				'order' => 'ASC',
				'start' => ($page - 1) * ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10),
				'limit' => ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10)
			);

			$bundle_products = $this->model_catalog_wk_bundle_product->getBundleProducts($filter_data);
			$bundle_products_total = $this->model_catalog_wk_bundle_product->getBundleProductsTotal();

		} else { //this is used for set layout of bundle products on any page such as home, product and checkout page.

			$data['page_layout'] = true;

			if(isset($this->request->get['product_id']) && $this->request->get['product_id']) {

				$bundle_products = $this->model_catalog_wk_bundle_product->getBundleProductsByID($this->request->get['product_id'], $this->config->get('module_wk_bundle_product_per_module'));

			} else {

				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}

				$filter_data = array(
					'sort'  => 'sort_order',
					'order' => 'ASC',
					'start' => ($page - 1) * ($this->config->get('module_wk_bundle_product_per_module') ? $this->config->get('module_wk_bundle_product_per_module') : 10),
					'limit' => ($this->config->get('module_wk_bundle_product_per_module') ? $this->config->get('module_wk_bundle_product_per_module') : 10)
				);

				$bundle_products = $this->model_catalog_wk_bundle_product->getBundleProducts($filter_data);
			}
		}		

		$data['bundle_products'] =array();

		foreach ($bundle_products as $key => $value) {

			$data['bundle_products'][$key]= $value;
			$products = array();
			$total = 0;
			$total_price = 0;
			$save_price = 0;
			$product_ids = '';

			foreach ($value['bundles'] as $index => $product) {


				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], 130, 130);
				} else {
					$image = $this->model_tool_image->resize('no_image.png', 130, 130);
				}

				if($product['minimum'] == 0) {
					$product['minimum'] = 1;
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {				
					$price = $this->currency->format($this->tax->calculate($product['price'] * $product['minimum'], $product['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$product['special']) {
					$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format(((float)$product['special'] ? $product['special'] : $product['price']),$this->session->data['currency']);
				} else {
					$tax = false;
				}

				$options = array();

				$this->load->model('catalog/product');

				foreach ($this->model_catalog_product->getProductOptions($product['product_id']) as $option) {

					$product_option_value_data = array();

					foreach ($option['product_option_value'] as $option_value) {
						if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {

							if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {

								$option_price = $this->currency->format($this->tax->calculate($option_value['price'], $product['tax_class_id'], $this->config->get('config_tax') ? 'P' : false),$this->session->data['currency']);

							} else {
								$option_price = false;
							}

							$product_option_value_data[] = array(
								'product_option_value_id' => $option_value['product_option_value_id'],
								'option_value_id'         => $option_value['option_value_id'],
								'name'                    => $option_value['name'],
								'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
								'price'                   => $option_price,
								'price_prefix'            => $option_value['price_prefix']
							);
						}
					}

					$options[] = array(
						'product_option_id'    => $option['product_option_id'],
						'product_option_value' => $product_option_value_data,
						'option_id'            => $option['option_id'],
						'name'                 => $option['name'],
						'type'                 => $option['type'],
						'value'                => $option['value'],
						'required'             => $option['required']
					);
				}

				$products[] = array(
				'product_id'  => $product['product_id'],
				'thumb'       => $image,
				'name'        => $product['name'],
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				'rating'      => $product['rating'],
				'minimum'     => $product['minimum'],
				'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id']),
				'options'     => $options
 				);

				if($special) {
				 	$total += ($product['special'] * $product['minimum']);
				 } else {
					$total += ($product['price'] * $product['minimum']);
				 }
				$total_price += ($product['price'] * $product['minimum']);
				$product_ids .= $product['product_id'].'_';
			}

			
			$data['bundle_products'][$key]['discount'] = $this->currency->format(((float)$value['discount']),$this->session->data['currency']);
			$data['bundle_products'][$key]['bundles'] = $products;
			$data['bundle_products'][$key]['total']   = $this->currency->format((float)$total,$this->session->data['currency']);
			$data['bundle_products'][$key]['total_price']   = $this->currency->format((float)$total_price,$this->session->data['currency']);
			$data['bundle_products'][$key]['bundle_product_id'] = $product_ids;
			$data['bundle_products'][$key]['bundle_href'] = $this->url->link('bundle/bundle', 'bundle_id=' .$value['bundle_id']);
			$data['bundle_products'][$key]['discount_per'] = $this->currency->format(((float)$value['discount']*(float)$total/100),$this->session->data['currency']);
			$final_price = 0;
			if ($data['bundle_products'][$key]['discount_option'] == 0) {
				$final_price = (float)$total-((float)$value['discount']*(float)$total/100);
				$data['bundle_products'][$key]['price'] = $this->currency->format(($final_price),$this->session->data['currency']);
			} else {
				$final_price = (float)$total-(float)$value['discount'];
				$data['bundle_products'][$key]['price'] = $this->currency->format(($final_price),$this->session->data['currency']);
			}
			$data['bundle_products'][$key]['save_price'] = $this->currency->format(((float)$total_price-((float)$final_price)),$this->session->data['currency']);

			if ($value['discount']>0) {
				$data['bundle_products'][$key]['check_discount'] = true;
			} else {
				$data['bundle_products'][$key]['check_discount'] = false;
			}
		}

		if(isset($data['page_layout']) && $data['page_layout'] )
		{
			return($this->load->view('extension/module/wk_bundle_layout', $data));

		} else {

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$pagination = new Pagination();
			$pagination->total = $bundle_products_total;
			$pagination->page = $page;
			$pagination->limit = ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10);
			$pagination->url = $this->url->link('extension/module/wk_bundle_product', 'page={page}', true);

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($bundle_products_total) ? (($page - 1) * ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10)) + 1 : 0, ((($page - 1) * ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10)) > ($bundle_products_total - ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10))) ? $bundle_products_total : ((($page - 1) * ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10)) + ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10)), $bundle_products_total, ceil($bundle_products_total / ($this->config->get('module_wk_bundle_product_per_page') ? $this->config->get('module_wk_bundle_product_per_page') : 10)));

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('extension/module/wk_bundle_product', $data));
		}
	}

	/**
	 * [addBundleToCart method is used for add bundle_id in the database and product related to
	 *  this bundle added to the cart.]
	 */
	public function addBundleToCart() {

		if(isset($this->request->post['bundle_id']) && $this->request->post['bundle_id'] && isset($this->request->post['product_id']) && $this->request->post['product_id']) {

			$this->load->model('catalog/wk_bundle_product');
			$this->model_catalog_wk_bundle_product->addBundleToCart($this->request->post['bundle_id'],$this->request->post['product_id']);

		}
	}

	/**
	 * [cartRemove method is used for clear all the products from cart.]
	 * @return [json] [it returns 0 products available in the cart.]
	 */
	public function cartRemove() {

		$this->load->model('catalog/wk_bundle_product');
		$this->load->language('extension/module/wk_bundle_product');
		$this->cart->clear();

		$json = array();

		if (!$this->cart->hasProducts()) {

			unset($this->session->data['vouchers']);

			$json['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			// Totals
			$this->load->model('setting/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));

			$this->model_catalog_wk_bundle_product->deleteBundleCart();

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}
}
?>