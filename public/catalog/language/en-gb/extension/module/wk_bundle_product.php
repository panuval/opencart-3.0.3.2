<?php
// Heading
$_['heading_title'] = 'Bundle Products';

// Entry
$_['entry_qty']     = 'Quantity';

// Text
$_['text_bundle_product'] 	= 'Bundle Products';
$_['text_bundles']  		= 'Bundles';
$_['text_option']   		= 'Available Options';
$_['text_select']   		= 'Select';
$_['text_loading']  		= 'Loading';
$_['text_price']    		= 'Price:';
$_['text_save']     		= 'You save :';
$_['minimum ']      		= 'Minimum';
$_['text_items']            = '%s item(s) - %s';
$_['text_remove']           = 'Success: You have modified your shopping cart!';

// Button
$_['button_clear_cart']  	= 'Clear Cart';

?>