<?php
/**
 * @author     Sainent eBusiness
 * @license    http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @version    1.0
 * @Website    http://www.sainent.com/
 */

$_['heading_title']     = 'PayU | %s';

$_['text_title']        = 'Credit Card / Debit Card / Net Banking / Wallet - PayU';
$_['text_success']      = '<span style="color: #37a000">Your payment was successfully received.</span>';
$_['text_unmappedstatus']= '<span style="color: #FF0000">Your payment has been </span>';
$_['text_failure']      = '<span style="color: #FF0000">Your payment has been failed</span>';
$_['text_cancelled']    = '<span style="color: #FF0000">Your payment has been cancelled</span>';
$_['text_pending']      = '<span style="color: #ff9b00">Your payment has been pending</span>';
$_['text_wait'] 		= '<b><span style="color: #FF0000">Please wait...</span></b><br>If you are not automatically re-directed in 10 seconds, please click <a href="%s">here</a>';										  

?>