<?php
// Text
$_['text_title']       = 'Shipping Options';
//$_['text_description'] = 'X-Shipping Pro';

$_['xshippingpro_estimator_header'] = 'Estimate Shipping Cost';
$_['xshippingpro_estimator_country'] = 'Select Delivery Country';
$_['xshippingpro_estimator_zone'] = 'Select Delivery Region / State';
$_['xshippingpro_estimator_postal'] = 'Enter Postal Code';
$_['xshippingpro_estimator_no_data'] = 'No shipping data is avilable';
$_['xshippingpro_estimator_button'] = 'Get Shipping Cost';
$_['xshippingpro_available'] = 'Yes, we ship our products to your location!';
$_['xshippingpro_no_available'] = 'Sorry, We do not ship to your location!';

$_['xshippingpro_select'] = '-Select-';
$_['xshippingpro_select_error'] = 'Please select an item';
$_['xshippingpro_product_error'] = 'We are very sorry, we cannot ship some of your added products. You must remove following products from your shopping cart to continue your order: <br /> %s';

?>