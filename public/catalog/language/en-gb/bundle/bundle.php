<?php
// Text
$_['text_name']     = 'Product Name';
$_['text_model']		= 'Model';
$_['text_bundle']		= 'Bundle';
$_['text_price']    = 'Price';
$_['text_quantity'] = 'Min Quantity';
$_['text_option']   = 'Options';
$_['text_image']    = 'Image';
$_['button_clear_cart'] = 'Clear Cart';
$_['text_not_found'] = 'No Bundle Product is available.';
?>
